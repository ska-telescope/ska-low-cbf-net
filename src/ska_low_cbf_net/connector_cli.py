# -*- coding: utf-8 -*-
#
# This file is part of the ska_low_cbf_net project
#
# Copyright (c) 2021 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

import argparse
import asyncio
import inspect
import logging
import os
import re
import readline
import signal
import socket
import sys
from cmd import Cmd

import yaml
from colors import color  # ansicolors

# from common import front_panel_regex, mac_address_regex, validate_ip
from ska_low_cbf_net.connector import Connector

mac_address_regex = re.compile(":".join(["[0-9a-fA-F]{2}"] * 6))
front_panel_regex = re.compile("([0-9]+)/([0-9]+)$")


def validate_ip(value):
    """Validate IP address string"""
    try:
        socket.inet_aton(value)
        return True
    except:
        return True


class CommandError(Exception):
    """Command failed"""

    pass


class Cli(Cmd):

    doc_header = "Available commands (type help <cmd>):"
    ruler = "="

    def __init__(
        self, controller, prompt="", stdin=None, use_rawinput=None, name=""
    ):
        super().__init__()
        self.log = logging.getLogger(__name__)
        self.ctrl = controller
        self.prompt = "{}>".format(prompt)
        self.name = name

        if stdin is not None:
            self.stdin = stdin
        if use_rawinput is not None:
            self.use_rawinput = use_rawinput

        # Remove -:/ from word delimiters
        delims = readline.get_completer_delims()
        delims = delims.replace("-", "")
        delims = delims.replace(":", "")
        delims = delims.replace("/", "")
        readline.set_completer_delims(delims)

    def out(self, message, clr=None):
        """Print output message"""
        print(color("\n{}\n".format(message), fg=clr))

    def doc(self, message):
        """Print command documentation"""
        self.out(message, "yellow")

    def error(self, message):
        """Print error message"""
        self.out(message, "red")

    def default(self, line):
        self.error("Unknown command: {}".format(line))
        return self.do_help("")

    def emptyline(self):
        # Do nothing
        pass

    def do_help(self, arg):
        """List available commands with 'help' or detailed help with 'help <cmd>'"""
        if arg:
            try:
                # Get help message from docstring
                method = getattr(self, "do_" + arg)
                doc_lines = inspect.getdoc(method).splitlines(True)
                if doc_lines:
                    # Insert command on top
                    doc_lines.insert(0, "{}\n".format(arg))
                    self.doc("  ".join(doc_lines))
                    return
            except AttributeError:
                pass
        # Use default help method
        super(Cli, self).do_help(arg)

    def cmdloop(self, intro=None):

        self.out(self.name)

        while True:
            try:
                super(Cli, self).cmdloop(intro="")
                break
            except KeyboardInterrupt:
                self.doc("Type exit to exit from {}".format(self.name))
            except Exception as e:
                self.log.exception(e)
                self.error("Unexpected error. See log for details")

    def run(self):
        self.cmdloop()

    def do_exit(self, line):
        """Quit the CLI"""

        self.out("Bye!")
        return True

    do_EOF = do_exit

    def do_history(self, line):
        """Show commands history"""

        if line:
            self.error("Unknown parameter: {}".format(line.strip()))
        else:
            msg = ""
            for i in range(readline.get_current_history_length() - 1):
                msg += "{}: {}\n".format(i, readline.get_history_item(i + 1))
            self.out(msg)

    # Commands
    def do_show_ports(self, line):
        """Show List all active ports and some associated metrics such as number of bytes/packets received and sent or
        number of errors. If a port number and its channel (i.e. 4/1 refers to port number 4, channel 1) is provided,
        only that port will be shown.
        """
        success, stats = self.ctrl.get_port_statistics(line)
        format_string = (
            "  {$PORT_NAME:^4} {$PORT_UP:^2} {$IS_VALID:^5} {$PORT_ENABLE:^7}"
            + " {$SPEED:^5} {$FEC:^4} {packets_sent:^16} {bytes_sent:^16}"
            + " {packets_received:^16} {bytes_received:^16}"
            + " {errors_received:^16} {errors_sent:^16}"
            + " {FCS_errors_received:^16}\n"
        )
        header = {
            "$PORT_NAME": "Port",
            "$PORT_UP": "Up",
            "$IS_VALID": "Valid",
            "$PORT_ENABLE": "Enabled",
            "$SPEED": "Speed",
            "$FEC": " FEC",
            "bytes_received": "Rx Bytes",
            "bytes_sent": "Tx Bytes",
            "packets_received": "Rx Packets",
            "packets_sent": "Tx Packets",
            "errors_received": "Rx Errors",
            "errors_sent": "Tx Errors",
            "FCS_errors_received": "FCS Errors",
        }

        msg = format_string.format(**header)
        if success:
            for v in stats:
                msg += format_string.format(**v)
            self.out(msg)
        else:
            self.error(stats)

    def do_show_basic_table(self, line=None):
        """Show the "Basic" table."""

        try:
            success, entries = self.ctrl.get_simple_table_entries()
            if not success:
                self.error(entries)

            if len(entries) == 0:
                self.out("No entries")
                return
            # print(entries)
            entries.sort(key=lambda x: x["port"])

            format_string = "  {ingress port:^7} {port:^7}\n"
            header = {"ingress port": "In Port", "port": "Out Port"}

            msg = format_string.format(**header)
            for v in entries:
                msg += format_string.format(**v)
            self.out(msg)

        except CommandError as e:
            self.error(e)

    def do_show_counter_ingress(self, line):
        """Show number of packets per protocol counters associated with ingress ports. If a port number is
        provided, only that port will be shown.
        Otherwise it will show only active ports
        """
        try:
            success, entries = self.ctrl.get_counter_ingress(line)
            if not success:
                self.error(entries)
                return
            if len(entries) == 0:
                self.out("No entries")
                return

            entries.sort(key=lambda x: x["Port"])

            format_string = "  {Port:^6} {Proto:^12} {Pkts:^32} {Bytes:^32}\n"
            header = {
                "Port": "Port #",
                "Proto": "Protocol",
                "Pkts": "number of packets",
                "Bytes": "number of bytes",
            }
            entries.sort(key=lambda x: x["Port"])
            total_entries = []
            pkts = 0
            bytes = 0
            for iterator, entry in enumerate(entries):
                pkts = pkts + entry["Pkts"]
                bytes = bytes + entry["Bytes"]
                if iterator % len(self.ctrl.protocol_names) is 0:
                    total_entries.append(
                        {
                            "Port": entry["Port"],
                            "Proto": "Total",
                            "Pkts": pkts,
                            "Bytes": bytes,
                        }
                    )
                    pkts = 0
                    bytes = 0
            for total_entry in total_entries:
                entries.append(total_entry)
            entries.sort(key=lambda x: x["Port"])

            msg = format_string.format(**header)
            for v in entries:
                msg += format_string.format(**v)
            self.out(msg)

        except CommandError as e:
            self.error(e)

    def do_show_counter_egress(self, line):
        """Show number of packets per protocol counters associated with egress ports. If a port number is provided,
        only that port will be shown.
        Otherwise it will show only active ports
        """
        try:
            success, entries = self.ctrl.get_counter_egress(line)
            if not success:
                self.error(entries)
                return
            if len(entries) == 0:
                self.out("No entries")
                return
            # print(entries)
            entries.sort(key=lambda x: x["Port"])

            format_string = "  {Port:^6} {Proto:^12} {Pkts:^32} {Bytes:^32}\n"
            header = {
                "Port": "Port #",
                "Proto": "Protocol",
                "Pkts": "number of packets",
                "Bytes": "number of bytes",
            }
            total_entries = []
            pkts = 0
            bytes = 0
            iterator = 0
            for entry in entries:
                iterator = iterator + 1
                pkts = pkts + entry["Pkts"]
                bytes = bytes + entry["Bytes"]
                if iterator % len(self.ctrl.protocol_names) is 0:
                    total_entries.append(
                        {
                            "Port": entry["Port"],
                            "Proto": "Total",
                            "Pkts": pkts,
                            "Bytes": bytes,
                        }
                    )
                    pkts = 0
                    bytes = 0
            for total_entry in total_entries:
                entries.append(total_entry)
            entries.sort(key=lambda x: x["Port"])

            msg = format_string.format(**header)
            for v in entries:
                msg += format_string.format(**v)
            self.out(msg)

        except CommandError as e:
            self.error(e)

    def do_reset_port_statistics(self, line):
        """
        Reset port statistics. These statistics relate to the general statistics of any active ports such as the
        number of bytes/packets received and sent or the number of errors detected in transmission and reception.
        These statistics are not related to the number of packets/bytes per port and per protocol given by the ingress
        and egress statistics.
        """
        self.ctrl.reset_ports_statistics()

    def do_reset_ingress_statistics(self, line):
        """
        Reset ingress counters
        """
        self.ctrl.reset_counter_ingress()

    def do_reset_egress_statistics(self, line):
        """
        Reset egress counters
        """
        self.ctrl.reset_counter_egress()

    def do_add_basic_table_entry(self, line):
        """Add “Basic” Table entry. The simple table matches a given ingress port to an egress port.
        This function create a entry and take two arguments, first the ingress then egress port.
        """
        try:
            if line:
                ports = line.split()
                if len(ports) is 2:
                    in_re_match = front_panel_regex.match(ports[0].strip())
                    out_re_match = front_panel_regex.match(ports[1].strip())
                    self.ctrl.add_simple_table_entry(in_re_match, out_re_match)
                else:
                    self.error(
                        "Unknown parameter: We need 2 parameters related to front panel name of port"
                    )
            else:
                self.error(
                    "Unknown parameter: We need 2 parameters related to front panel name of port"
                )
        except CommandError as e:
            self.error(e)

    def do_remove_basic_table_entry(self, line):
        """Add “Basic” Table entry. The simple table matches a given ingress port to an egress port.
        This function create a entry and take two arguments, first the ingress then egress port.
        """
        try:
            if line:
                ports = line.split()
                if len(ports) is 1:
                    in_re_match = front_panel_regex.match(ports[0].strip())
                    self.ctrl.del_simple_table_entry(in_re_match)
                else:
                    self.error(
                        "Unknown parameter: We need 2 parameters related to front panel name of port"
                    )
            else:
                self.error(
                    "Unknown parameter: We need 2 parameters related to front panel name of port"
                )
        except CommandError as e:
            self.error(e)

    def do_update_simple_table_entry(self, line):
        """Update "Basic" Table entry. The simple table matches a given ingress port to an egress port.
        This function updates an entry and takes two arguments, first the ingress then egress port.
        """
        try:
            if line:
                ports = line.split()
                if len(ports) is 2:
                    in_re_match = front_panel_regex.match(ports[0].strip())
                    out_re_match = front_panel_regex.match(ports[1].strip())
                    self.ctrl.update_simple_table_entry(
                        in_re_match, out_re_match
                    )
                else:
                    self.error(
                        "Unknown parameter: We need 2 parameters related to front panel name of port"
                    )
            else:
                self.error(
                    "Unknown parameter: We need 2 parameters related to front panel name of port"
                )
        except CommandError as e:
            self.error(e)

    def do_load_port(self, line):
        """Load ports yaml file and enable switch ports.

        Keyword arguments:
            line -- yaml file name

        Returns:
            (success flag, list of ports or error message)
        """
        ports = []
        fib = {}
        file_name = line.strip()
        self.out(file_name)
        with open(file_name) as f:
            yaml_ports = yaml.safe_load(f)

            for port, value in yaml_ports["ports"].items():

                re_match = front_panel_regex.match(port)
                if not re_match:
                    return (False, "Invalid port {}".format(port))

                fp_port = int(re_match.group(1))
                fp_lane = int(re_match.group(2))

                # Convert all keys to lowercase
                value = {k.lower(): v for k, v in value.items()}

                if "speed" in value:
                    try:
                        speed = int(
                            value["speed"].upper().replace("G", "").strip()
                        )
                    except ValueError:
                        return (
                            False,
                            "Invalid speed for port {}".format(port),
                        )

                    if speed not in [10, 25, 40, 50, 100]:
                        return (
                            False,
                            "Port {} speed must be one of 10G,25G,40G,50G,100G".format(
                                port
                            ),
                        )
                else:
                    speed = 100

                if "fec" in value:
                    fec = value["fec"].lower().strip()
                    if fec not in ["none", "fc", "rs"]:
                        return (
                            False,
                            "Port {} fec must be one of none, fc, rs".format(
                                port
                            ),
                        )
                else:
                    fec = "none"

                if "autoneg" in value:
                    an = value["autoneg"].lower().strip()
                    if an not in ["default", "enable", "disable"]:
                        return (
                            False,
                            "Port {} autoneg must be one of default, enable, disable".format(
                                port
                            ),
                        )
                else:
                    an = "default"

                if "mac" not in value:
                    return (
                        False,
                        "Missing MAC address for port {}".format(port),
                    )

                success, dev_port = self.ctrl.ports.get_dev_port(
                    fp_port, fp_lane
                )
                if success:
                    fib[dev_port] = value["mac"].upper()
                else:
                    return (False, dev_port)

                ports.append((fp_port, fp_lane, speed, fec, an))
        success, error_msg = self.ctrl.load_ports(ports)
        if not success:
            self.error(error_msg)

    def do_create_multicast_session(self, line):
        """Create a multicast session  .

        Keyword arguments:
            line -- session id

        Returns:
            (success flag, list of ports or error message)
        """
        try:
            if line:
                ports = line.split()
                if len(ports) is 1:
                    session = int(ports[0].strip())
                    self.ctrl.create_muticast_session(session)
                else:
                    self.error(
                        "Unknown parameter: We need 1 parameter related to front panel name of port"
                    )
            else:
                self.error(
                    "Unknown parameter: We need 1 parameter related to front panel name of port"
                )
        except CommandError as e:
            self.error(e)

    def do_configure_multicast_session(self, line):
        """Configure a multicast session  .

        Keyword arguments:
            line -- session id, rid, yid, brid, hash1, hash2

        Returns:
            (success flag, list of ports or error message)
        """
        try:
            if line:
                ports = line.split()
                if len(ports) is 6:
                    session = int(ports[0].strip())
                    rid = int(ports[1].strip())
                    yid = int(ports[2].strip())
                    brid = int(ports[3].strip())
                    hash1 = int(ports[4].strip(), 16)
                    hash2 = int(ports[5].strip(), 16)
                    self.ctrl.configure_multicast_session(
                        session, rid, yid, brid, hash1, hash2
                    )
                else:
                    self.error(
                        "Unknown parameter: We need 6 parameter related to front panel name of port"
                    )
            else:
                self.error(
                    "Unknown parameter: We need 6 parameter related to front panel name of port"
                )
        except CommandError as e:
            self.error(e)

    def do_add_ingress_to_multicast_session(self, line):
        """associate an ingress port to a multicast session.

        Keyword arguments:
            line -- ingress, session

        Returns:
            (success flag, list of ports or error message)
        """
        try:
            if line:
                ports = line.split()
                if len(ports) is 2:
                    ingress = front_panel_regex.match(ports[0].strip())
                    session = int(ports[1].strip())
                    self.ctrl.add_ingress_to_multicast_session(
                        ingress, session
                    )
                else:
                    self.error(
                        "Unknown parameter: We need 2 parameter related to front panel name of port"
                    )
            else:
                self.error(
                    "Unknown parameter: We need 2 parameter related to front panel name of port"
                )
        except CommandError as e:
            self.error(e)

    def do_remove_ingress_to_multicast_session(self, line):
        """remove an ingress port.

        Keyword arguments:
            line -- ingress

        Returns:
            (success flag, list of ports or error message)
        """
        try:
            if line:
                ports = line.split()
                if len(ports) is 1:
                    ingress = front_panel_regex.match(ports[0].strip())
                    self.ctrl.remove_ingress_from_multicast_session(ingress)
                else:
                    self.error(
                        "Unknown parameter: We need 1 parameter related to front panel name of port"
                    )
            else:
                self.error(
                    "Unknown parameter: We need 1 parameter related to front panel name of port"
                )
        except CommandError as e:
            self.error(e)

    def do_add_ports_to_multicast_session(self, line):
        """Add destination ports in multicast session .

        Keyword arguments:
            line -- session, file containing list of (rid, port)

        Returns:
            (success flag, list of ports or error message)
        """
        pass

    def do_add_port_to_multicast_session(self, line):
        """Add destination port in multicast session.

        Keyword arguments:
            line -- session, rid, port

        Returns:
            (success flag, list of ports or error message)
        """
        try:
            if line:
                ports = line.split()
                if len(ports) is 3:
                    session = int(ports[0].strip())
                    rid = int(ports[1].strip())
                    port = front_panel_regex.match(ports[2].strip())
                    self.ctrl.add_port_to_multicast_session(session, rid, port)
                else:
                    self.error(
                        "Unknown parameter: We need 3 parameter related to front panel name of port"
                    )
            else:
                self.error(
                    "Unknown parameter: We need 3 parameter related to front panel name of port"
                )
        except CommandError as e:
            self.error(e)

    def do_remove_port_to_multicast_session(self, line):
        """Remove destination port in multicast session.

        Keyword arguments:
            line -- session, rid, port

        Returns:
            (success flag, list of ports or error message)
        """
        try:
            if line:
                ports = line.split()
                if len(ports) is 3:
                    session = int(ports[0].strip())
                    rid = int(ports[1].strip())
                    port = front_panel_regex.match(ports[2].strip())
                    self.ctrl.remove_port_from_multicast_session(
                        session, rid, port
                    )
                else:
                    self.error(
                        "Unknown parameter: We need 1 parameter related to front panel name of port"
                    )
            else:
                self.error(
                    "Unknown parameter: We need 1 parameter related to front panel name of port"
                )
        except CommandError as e:
            self.error(e)

    def do_start_prometheus_telemetry(self, line):
        """start the telemetry thread using connector telemetry  .



        Returns:
            (success flag, list of ports or error message)
        """

    def do_get_multicast_sessions_and_port(self, line):
        """get information about multicast sessions
        Returns:
            (success flag, list of ports or error message)
        """
        session_port = self.ctrl.get_multicast_configurations()

        print(session_port)

    def do_show_arp_counter(self, line):
        """Show Counters from ARP table."""
        try:
            success, entries = self.ctrl.get_arp_table_counters()
            if not success:
                self.error(entries)
                return
            if len(entries) == 0:
                self.out("No entries")
                return

            entries.sort(key=lambda x: x["IP"])

            format_string = "  {IP:^12} {Pkts:^32} {Bytes:^32}\n"
            header = {
                "IP": "IP@",
                "Pkts": "number of packets",
                "Bytes": "number of bytes",
            }

            msg = format_string.format(**header)
            for v in entries:
                msg += format_string.format(**v)
            self.out(msg)

        except CommandError as e:
            self.error(e)

    def do_show_arp_table(self, line):
        """Show the ARP table."""
        try:
            success, entries = self.ctrl.get_arp_table_entries()
            if not success:
                self.error(entries)
            if len(entries) == 0:
                self.out("No entries")
                return
            entries.sort(key=lambda x: x["IP"])

            format_string = "  {IP:^12} {Mac:^12}\n"
            header = {
                "IP": "IP@",
                "Mac": "Mac@",
            }

            msg = format_string.format(**header)
            for v in entries:
                msg += format_string.format(**v)
            self.out(msg)

        except CommandError as e:
            self.error(e)

    def do_add_arp_table_entry(self, line):
        """
        Add ARP Table entry. The simple table matches a given IP address  to an MAC address.
        This function creates an entry and takes two arguments, first the IP address then MAC address
        """
        try:
            if line:
                ports = line.split()
                if len(ports) is 2:
                    ip = ports[0].strip()
                    print(ip)
                    mac = ports[1].strip()
                    print(mac)
                    self.ctrl.add_arp_table_entry(ip, mac)
                else:
                    self.error(
                        "Unknown parameter: We need 2 parameters related to "
                        "front panel name of port"
                    )
            else:
                self.error(
                    "Unknown parameter: We need 2 parameters related to "
                    "front panel name of port"
                )
        except CommandError as e:
            self.error(e)


def launch_cli(cli_class=Cli, connector_class=Connector):
    # Parse arguments
    argparser = argparse.ArgumentParser(description="Perentie controller.")
    argparser.add_argument(
        "--program",
        type=str,
        default="tna_telemetry",
        help="P4 program name. Default: tna_telemetry",
    )
    argparser.add_argument(
        "--bfrt-ip",
        type=str,
        default="127.0.0.1",
        help="Name/address of the BFRuntime server. Default: 127.0.0.1",
    )
    argparser.add_argument(
        "--bfrt-port",
        type=int,
        default=50052,
        help="Port of the BFRuntime server. Default: 50052",
    )
    argparser.add_argument(
        "--client-id",
        type=int,
        default=0,
        help="Client ID to connect to the switch. Default: 0",
    )
    argparser.add_argument(
        "--ports",
        type=str,
        default="ports.yaml",
        help="YAML file describing connections to ports. Default: ports.yaml",
    )
    argparser.add_argument(
        "--log-level",
        default="INFO",
        choices=["ERROR", "WARNING", "INFO", "DEBUG"],
        help="Default: INFO",
    )
    args = argparser.parse_args()
    # Configure logging
    numeric_level = getattr(logging, args.log_level.upper(), None)
    if not isinstance(numeric_level, int):
        sys.exit("Invalid log level: {}".format(args.log_level))
    logformat = "%(asctime)s - %(levelname)s - %(message)s"
    logging.basicConfig(
        filename="perentie.log",
        filemode="w",
        level=numeric_level,
        format=logformat,
        datefmt="%H:%M:%S",
    )
    args.bfrt_ip = args.bfrt_ip.strip()
    if not validate_ip(args.bfrt_ip):
        sys.exit("Invalid BFRuntime server IP address")
    ctrl = connector_class(
        args.program, args.bfrt_ip, args.bfrt_port, args.client_id
    )
    cli = cli_class(ctrl, prompt="Perentie", name="Perentie controller")
    event_loop = asyncio.get_event_loop()
    # Start controller
    cli.run()
    event_loop.close()
    # Flush log, stdout, stderr
    sys.stdout.flush()
    sys.stderr.flush()
    logging.shutdown()
    # Exit
    sys.exit(0)


if __name__ == "__main__":
    launch_cli()
