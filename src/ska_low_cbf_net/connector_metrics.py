import logging


class Metrics(object):
    def __init__(self, target, gc, ports):
        super(Metrics, self).__init__()
        # Set up base class

        self.log = logging.getLogger(__name__)
        # Keep a trace of the configured ports
        self.gc = gc
        self.ports = ports
        self.target = target

    def _clear(self):
        """Remove all entries"""
        pass

    def reset_all(self):
        """Reset all metric

        Keyword arguments:
            action -- action to act on a given match
            match -- match in the packet header
        """
        pass

    def reset_single(self, metric):
        """Add entries.

        Keyword arguments:
            metric -- a single metric
        """
        pass

    def get_metrics(self):
        """Get all metrics entries.

        Returns:
            list of metrics
        """
        pass

    def get_metric(self, metric):
        """Get all metrics entries.
        Keyword arguments:
            metric -- a single metric
        Returns:
            value of a given metric
        """
        pass
