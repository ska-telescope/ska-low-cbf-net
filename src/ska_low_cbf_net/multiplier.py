#  Copyright 2021 CSIRO
#
#  Class to control multiplification of traffic in the Perentie P4 code


import logging

from bfrt_grpc.bfruntime_pb2 import TableModIncFlag


class Multiplier(object):
    def __init__(self, target, gc, bfrt_info):
        # Set up base class

        self.log = logging.getLogger(__name__)

        # session, matching a given port to a session number
        self.session_table = bfrt_info.table_get("ing_port")

        # configuring the session, for a session id need {ReplicationId_t rid, bit<9> yid, bit<16> brid, bit<13> hash1, bit<13> hash2}
        #  Example
        #  rid: '1'
        #      yid: '1'
        #      brid: '1'
        #      hash1: '0x1FFF'
        #      hash2: '0x1EEE'
        self.configure_session = bfrt_info.table_get("ing_src_ifid")

        self.multiplier_group = bfrt_info.table_get("$pre.mgid")
        self.node = bfrt_info.table_get("$pre.node")

        # configuring session id to flood
        self.flooding = bfrt_info.table_get("ing_dmac")

        self.gc = gc
        self.target = target
        self.session_to_ports = self.get_sessions_and_ports()

    def _clear(self):
        """Remove all entries"""
        resp = self.session_table.entry_get(self.target)
        for _, k in resp:
            self.session_table.entry_del(self.target, [k])

        resp = self.configure_session.entry_get(self.target)
        for _, k in resp:
            self.configure_session.entry_del(self.target, [k])

        resp = self.flooding.entry_get(self.target)
        for _, k in resp:
            self.flooding.entry_del(self.target, [k])
        resp = self.multiplier_group.entry_get(self.target)
        for _, k in resp:
            self.multiplier_group.entry_del(self.target, [k])

        resp = self.node.entry_get(self.target)
        for _, k in resp:
            self.node.entry_del(self.target, [k])

    def clear(self):
        self._clear()

    def create_multicast_session(self, session):

        self.multiplier_group.entry_add(
            self.target,
            [
                self.multiplier_group.make_key(
                    [self.gc.KeyTuple("$MGID", session)]
                )
            ],
            [
                self.multiplier_group.make_data(
                    [
                        self.gc.DataTuple(
                            "$MULTICAST_NODE_ID", int_arr_val=[]
                        ),
                        self.gc.DataTuple(
                            "$MULTICAST_NODE_L1_XID_VALID", bool_arr_val=[]
                        ),
                        self.gc.DataTuple(
                            "$MULTICAST_NODE_L1_XID", int_arr_val=[]
                        ),
                    ]
                )
            ],
        )

    def add_port_to_multicast_session(self, session, rid, port):
        """Create a multicast node and add it to a multicast group.

        Keyword arguments:
            session -- multicast group ID
            rid -- node ID
            port -- device port for the node

        Returns:
            (success flag, None or error message)
        """
        # Check if multicast group exists
        found, msg = self.check_session_id(session)

        if not found:
            self.log.error(msg)
            return (False, msg)

        # Check if a node with the same ID exists
        resp = self.node.entry_get(self.target, flags={"from_hw": False})
        for _, k in resp:
            if k.to_dict()["$MULTICAST_NODE_ID"]["value"] == rid:
                error_msg = "Multicast node {} already present".format(rid)
                self.log.error(error_msg)
                return (False, error_msg)

        # Add node
        self.node.entry_add(
            self.target,
            [
                self.node.make_key(
                    [self.gc.KeyTuple("$MULTICAST_NODE_ID", rid)]
                )
            ],
            [
                self.node.make_data(
                    [
                        self.gc.DataTuple("$MULTICAST_RID", rid),
                        self.gc.DataTuple("$DEV_PORT", int_arr_val=[port]),
                    ]
                )
            ],
        )

        # Extend multiplier group
        self.multiplier_group.entry_mod_inc(
            self.target,
            [
                self.multiplier_group.make_key(
                    [self.gc.KeyTuple("$MGID", session)]
                )
            ],
            [
                self.multiplier_group.make_data(
                    [
                        self.gc.DataTuple(
                            "$MULTICAST_NODE_ID", int_arr_val=[rid]
                        ),
                        self.gc.DataTuple(
                            "$MULTICAST_NODE_L1_XID_VALID", bool_arr_val=[True]
                        ),
                        self.gc.DataTuple(
                            "$MULTICAST_NODE_L1_XID", int_arr_val=[rid]
                        ),
                    ]
                )
            ],
            TableModIncFlag.MOD_INC_ADD,
        )
        return (True, None)

    def remove_port_from_multicast_session(self, session, rid):
        """
        Remove a multicast node and add it to a multicast group.

        Keyword arguments:
            session -- multicast group ID
            rid -- node ID

        Returns:
            (success flag, None or error message)
        """
        # Check if multicast group exists
        found, msg = self.check_session_id(session)

        if not found:
            self.log.error(msg)
            return (False, msg)

        # Remove port in multiplier group
        self.multiplier_group.entry_mod_inc(
            self.target,
            [
                self.multiplier_group.make_key(
                    [self.gc.KeyTuple("$MGID", session)]
                )
            ],
            [
                self.multiplier_group.make_data(
                    [
                        self.gc.DataTuple(
                            "$MULTICAST_NODE_ID", int_arr_val=[rid]
                        ),
                        self.gc.DataTuple(
                            "$MULTICAST_NODE_L1_XID_VALID", bool_arr_val=[True]
                        ),
                        self.gc.DataTuple(
                            "$MULTICAST_NODE_L1_XID", int_arr_val=[rid]
                        ),
                    ]
                )
            ],
            TableModIncFlag.MOD_INC_DELETE,
        )

        # Remove node
        self.node.entry_del(
            self.target,
            [
                self.node.make_key(
                    [self.gc.KeyTuple("$MULTICAST_NODE_ID", rid)]
                )
            ],
        )

        return (True, None)

    def add_ports_to_multicast_session(self, session, rid_ports):
        """add a list of port to a multicast session.
        Keyword arguments:
            session -- multicast group ID
            rid_ports -- list of tuples (node ID, device port)
        Returns:
            success indication
        """
        for rid, port in rid_ports:
            success, error_msg = self.add_port_to_multicast_session(
                session, rid, port
            )
            if not success:
                return (False, error_msg)
        return (True, None)

    def associate_ingress_to_multicast(self, ingress, session):
        """associate an ingress port to be multicasted in a multicast session.

        Returns:
            success indication
        """
        found, msg = self.check_session_id(session)

        if not found:
            self.log.error(msg)
            return (False, msg)
        self.session_table.entry_add(
            self.target,
            [
                self.session_table.make_key(
                    [self.gc.KeyTuple("ingress_port", ingress)]
                )
            ],
            [
                self.session_table.make_data(
                    [self.gc.DataTuple("ifid", session)], "set_ifid"
                )
            ],
        )

    def remove_ingress_from_multicast(self, ingress):
        """remove an ingress port to be multicasted in a multicast session."""
        try:
            self.session_table.entry_del(
                self.target,
                [
                    self.session_table.make_key(
                        [self.gc.KeyTuple("ingress_port", ingress)]
                    )
                ],
            )
        except CommandError as e:
            return False, e
        return True, "Port removed"

    def configure_multicast_session(
        self, session, rid, yid, brid, hash1, hash2
    ):
        """configure session ID and enable multicast in the switch

        Returns:
            success indication
        """
        found, msg = self.check_session_id(session)

        if not found:
            self.log.error(msg)
            return (False, msg)
        self.configure_session.entry_add(
            self.target,
            [
                self.configure_session.make_key(
                    [self.gc.KeyTuple("ig_md.ifid", session)]
                )
            ],
            [
                self.configure_session.make_data(
                    [
                        self.gc.DataTuple("rid", rid),
                        self.gc.DataTuple("yid", yid),
                        self.gc.DataTuple("brid", brid),
                        self.gc.DataTuple("hash1", hash1),
                        self.gc.DataTuple("hash2", hash2),
                    ],
                    "set_src_ifid_md",
                )
            ],
        )
        self.flooding.entry_add(
            self.target,
            [self.flooding.make_key([self.gc.KeyTuple("ig_md.brid", brid)])],
            [self.flooding.make_data([], "flood")],
        )

    def check_session_id(self, session):

        resp = self.multiplier_group.entry_get(
            self.target, flags={"from_hw": False}
        )
        found = False
        for _, k in resp:
            if session == k.to_dict()["$MGID"]["value"]:

                found = True
        if not found:
            error_msg = "Multicast group {} not present".format(session)
            self.log.error(error_msg)
            return (False, error_msg)

        return (True, None)

    def get_sessions_and_ports(self):
        """To get all sessions configured and their associated ports


        :return:
        """
        entries = {}
        entries_from_switch = self.multiplier_group.entry_get(self.target)
        rids_to_port = {}
        nodes = self.node.entry_get(self.target)
        for data, key in nodes:
            rids_to_port[
                key.to_dict()["$MULTICAST_NODE_ID"]["value"]
            ] = data.to_dict()["$DEV_PORT"][0]
        for data, key in entries_from_switch:
            rids = data.to_dict()["$MULTICAST_NODE_ID"]
            ports = []
            for rid in rids:
                ports.append(rids_to_port[rid])
            entries[str(key.to_dict()["$MGID"]["value"])] = ports
        return entries

    def get_entries(self):
        """Get all forwarding entries.

        Returns:
            list of (frequency_no, dev port)
        """
        entries = {}
        entries_from_switch = self.session_table.entry_get(self.target)
        for data, key in entries_from_switch:
            # print(key.to_dict()['ingress_port']['value'])
            # print(data.to_dict()['dest_port'])
            entries[key.to_dict()["frequency_no"]["value"]] = data.to_dict()[
                "dest_port"
            ]

        return entries
