"""
Tests for Connector class
"""
import re

import pytest
import yaml
from async_packet_test.predicates import (
    Predicate,
    did_not_see_packet_equaling,
    saw_packet_equaling,
)
from scapy.all import ARP, IP, UDP, Ether, Raw, sendp

from .spead_packet import Spead

front_panel_regex = re.compile("([0-9]+)/([0-9]+)$")

packet = (
    Ether()
    / IP()
    / UDP(sport=4550, dport=4660)
    / Spead(frequency_no=1, beam_no=2, sub_array=3)
)
packet_update = (
    Ether()
    / IP()
    / UDP(sport=4551, dport=4660)
    / Spead(frequency_no=1, beam_no=2, sub_array=3)
)

arp_querry = Ether(src="00:11:22:33:44:55", dst="ff:ff:ff:ff:ff:ff") / ARP(
    pdst="192.168.1.1",
    op=1,
    hwsrc="00:11:22:33:44:55",
    hwdst="ff:ff:ff:ff:ff:ff",
    psrc="192.168.1.2",
)

arp_response = (
    Ether(src="aa:bb:cc:dd:ee:ff", dst="00:11:22:33:44:55")
    / ARP(
        psrc="192.168.1.1",
        op=2,
        hwdst="00:11:22:33:44:55",
        hwsrc="aa:bb:cc:dd:ee:ff",
        hwlen=6,
        pdst="192.168.1.2",
    )
    / Raw(
        "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
    )
)

arp_response_update = (
    Ether(src="aa:bb:cc:dd:ee:11", dst="00:11:22:33:44:55")
    / ARP(
        psrc="192.168.1.1",
        op=2,
        hwdst="00:11:22:33:44:55",
        hwsrc="aa:bb:cc:dd:ee:11",
        hwlen=6,
        pdst="192.168.1.2",
    )
    / Raw(
        "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
    )
)


def test_nothing():
    pass


@pytest.mark.bfrt
@pytest.mark.usefixtures("context", "ctrl")
class TestPortManagement:
    def test_reset_and_load_ports(self, context, ctrl):
        # we are going to reset the physical port and reload them
        # checking that the staged configurations are correct

        ctrl.remove_ports()

        ports = []
        fib = {}

        success, result = ctrl.get_port_statistics()
        assert not success

        with open("./tests/tests/ports.yaml") as f:
            yaml_ports = yaml.safe_load(f)

            for port, value in yaml_ports["ports"].items():

                re_match = front_panel_regex.match(port)
                if not re_match:
                    return (False, "Invalid port {}".format(port))

                fp_port = int(re_match.group(1))
                fp_lane = int(re_match.group(2))

                # Convert all keys to lowercase
                value = {k.lower(): v for k, v in value.items()}

                if "speed" in value:
                    try:
                        speed = int(
                            value["speed"].upper().replace("G", "").strip()
                        )
                    except ValueError:
                        return (
                            False,
                            "Invalid speed for port {}".format(port),
                        )

                    if speed not in [10, 25, 40, 50, 100]:
                        return (
                            False,
                            "Port {} speed must be one of 10G,25G,40G,50G,100G".format(
                                port
                            ),
                        )
                else:
                    speed = 100

                if "fec" in value:
                    fec = value["fec"].lower().strip()
                    if fec not in ["none", "fc", "rs"]:
                        return (
                            False,
                            "Port {} fec must be one of none, fc, rs".format(
                                port
                            ),
                        )
                else:
                    fec = "none"

                if "autoneg" in value:
                    an = value["autoneg"].lower().strip()
                    if an not in ["default", "enable", "disable"]:
                        return (
                            False,
                            "Port {} autoneg must be one of default, enable, disable".format(
                                port
                            ),
                        )
                else:
                    an = "default"

                if "mac" not in value:
                    return (
                        False,
                        "Missing MAC address for port {}".format(port),
                    )

                success, dev_port = ctrl.ports.get_dev_port(fp_port, fp_lane)
                if success:
                    fib[dev_port] = value["mac"].upper()
                else:
                    return (False, dev_port)

                ports.append((fp_port, fp_lane, speed, fec, an))
        success, error_msg = ctrl.load_ports(ports)
        assert success

    @pytest.mark.skip("Fails in emulated switch")
    def test_port_statistics(self, context, ctrl):
        result = context.expect("veth4", saw_packet_equaling(packet))
        ctrl.reset_ports_statistics()
        ctrl.clear_simple_table()
        ctrl.add_simple_table_entry(
            front_panel_regex.match("1/1"), front_panel_regex.match("1/2")
        )

        sendp(packet, iface="veth2")
        result.assert_true()
        success, result1 = ctrl.get_port_statistics("1/1")
        expected_result = []

        expected_result.append(
            {
                "$AUTO_NEGOTIATION": "PM_AN_FORCE_DISABLE",
                "$CHNL_ID": 1,
                "$CONN_ID": 1,
                "$CUT_THROUGH_EN": False,
                "$FEC": "NONE",
                "$IS_INTERNAL": False,
                "$IS_VALID": True,
                "$LOOPBACK_MODE": "BF_LPBK_NONE",
                "$MEDIA_TYPE": "BF_MEDIA_TYPE_UNKNOWN",
                "$N_LANES": 1,
                "$PORT_DIR": "PM_PORT_DIR_DEFAULT",
                "$PORT_ENABLE": True,
                "$PORT_NAME": "1/1",
                "$PORT_UP": False,
                "$RX_MTU": 10240,
                "$RX_PAUSE_FRAME_EN": False,
                "$RX_PFC_EN_MAP": 0,
                "$SDS_TX_ATTN": 0,
                "$SDS_TX_POST": 0,
                "$SDS_TX_POST2": 0,
                "$SDS_TX_PRE": 0,
                "$SDS_TX_PRE2": 0,
                "$SPEED": "10G",
                "$TX_MTU": 10240,
                "$TX_PAUSE_FRAME_EN": False,
                "$TX_PFC_EN_MAP": 0,
                "action_name": None,
                "is_default_entry": False,
                "$DEV_PORT": 1,
                "bytes_received": 1404858,
                "packets_received": 189,
                "errors_received": 0,
                "FCS_errors_received": 0,
                "bytes_sent": 8114,
                "packets_sent": 1,
                "errors_sent": 0,
            }
        )
        assert result1 == expected_result
        success, result2 = ctrl.get_port_statistics("1/2")
        expected_result2 = []

        expected_result2.append(
            {
                "$AUTO_NEGOTIATION": "PM_AN_FORCE_DISABLE",
                "$CHNL_ID": 2,
                "$CONN_ID": 1,
                "$CUT_THROUGH_EN": False,
                "$FEC": "NONE",
                "$IS_INTERNAL": False,
                "$IS_VALID": True,
                "$LOOPBACK_MODE": "BF_LPBK_NONE",
                "$MEDIA_TYPE": "BF_MEDIA_TYPE_UNKNOWN",
                "$N_LANES": 1,
                "$PORT_DIR": "PM_PORT_DIR_DEFAULT",
                "$PORT_ENABLE": True,
                "$PORT_NAME": "1/2",
                "$PORT_UP": False,
                "$RX_MTU": 10240,
                "$RX_PAUSE_FRAME_EN": False,
                "$RX_PFC_EN_MAP": 0,
                "$SDS_TX_ATTN": 0,
                "$SDS_TX_POST": 0,
                "$SDS_TX_POST2": 0,
                "$SDS_TX_PRE": 0,
                "$SDS_TX_PRE2": 0,
                "$SPEED": "10G",
                "$TX_MTU": 10240,
                "$TX_PAUSE_FRAME_EN": False,
                "$TX_PFC_EN_MAP": 0,
                "action_name": None,
                "is_default_entry": False,
                "$DEV_PORT": 2,
                "bytes_received": 308500,
                "packets_received": 38,
                "errors_received": 0,
                "FCS_errors_received": 0,
                "bytes_sent": 8114,
                "packets_sent": 1,
                "errors_sent": 0,
            }
        )
        assert result2 == expected_result2


# Note these tests fail unless you have a real barefoot runtime
# (and P4 switch?)
@pytest.mark.bfrt
@pytest.mark.usefixtures("context", "ctrl")
class TestSimple:
    def test_saw_packet_simple_table(self, context, ctrl):
        result = context.expect("veth6", saw_packet_equaling(packet))
        ctrl.clear_simple_table()
        ctrl.add_simple_table_entry(
            front_panel_regex.match("1/1"), front_panel_regex.match("1/3")
        )
        success, table = ctrl.get_simple_table_entries()
        expected_table = []
        expected_table.append({"ingress port": "1/1", "port": "1/3"})
        assert table == expected_table
        sendp(packet, iface="veth2")
        result.assert_true()
        ctrl.clear_simple_table()
        expected_table = []
        success, table = ctrl.get_simple_table_entries()
        assert table == expected_table
        ctrl.add_simple_table_entry(
            front_panel_regex.match("1/1"), front_panel_regex.match("1/3")
        )
        ctrl.del_simple_table_entry(front_panel_regex.match("1/1"))
        expected_table = []
        success, table = ctrl.get_simple_table_entries()
        assert table == expected_table

    def test_did_not_see_packet_simple_table(self, context, ctrl):

        ctrl.clear_simple_table()
        ctrl.add_simple_table_entry(
            front_panel_regex.match("1/1"), front_panel_regex.match("1/3")
        )
        results = [
            context.expect("veth0", did_not_see_packet_equaling(packet)),
            context.expect("veth4", did_not_see_packet_equaling(packet)),
            context.expect("veth8", did_not_see_packet_equaling(packet)),
            context.expect("veth10", did_not_see_packet_equaling(packet)),
            context.expect("veth12", did_not_see_packet_equaling(packet)),
            context.expect("veth14", did_not_see_packet_equaling(packet)),
        ]
        sendp(packet, iface="veth2")

        for result in results:
            assert result

    def test_saw_packet_and_update_basic_table(self, context, ctrl):

        result = context.expect("veth0", saw_packet_equaling(packet))
        ctrl.clear_simple_table()
        ctrl.add_simple_table_entry(
            front_panel_regex.match("1/1"), front_panel_regex.match("1/0")
        )
        sendp(packet, iface="veth2")
        result.assert_true()
        result_update = context.expect(
            "veth4", saw_packet_equaling(packet_update)
        )
        ctrl.update_simple_table_entry(
            front_panel_regex.match("1/1"), front_panel_regex.match("1/2")
        )
        sendp(packet_update, iface="veth2")
        result_update.assert_true()
        ctrl.clear_simple_table()


@pytest.mark.bfrt
@pytest.mark.usefixtures("context", "ctrl")
class TestARP:
    def test_saw_arp_table(self, context, ctrl):
        result = context.expect("veth2", saw_packet_equaling(arp_response))
        ctrl.clear_arp_table()
        ctrl.add_arp_table_entry("192.168.1.1", "aa:bb:cc:dd:ee:ff")
        success, table = ctrl.get_arp_table_entries()

        expected_table = []
        expected_table.append(
            {"IP": "192.168.1.1", "Mac": "aa:bb:cc:dd:ee:ff"}
        )
        assert table == expected_table
        sendp(arp_querry, iface="veth2")
        result.assert_true()
        ctrl.clear_arp_table()
        expected_table = []
        success, table = ctrl.get_arp_table_entries()
        assert table == expected_table
        ctrl.add_arp_table_entry("192.168.1.1", "aa:bb:cc:dd:ee:ff")
        ctrl.del_arp_table_entry("192.168.1.1", "aa:bb:cc:dd:ee:ff")
        expected_table = []
        success, table = ctrl.get_arp_table_entries()
        assert table == expected_table

    def test_saw_packet_and_update_arp_table(self, context, ctrl):

        result = context.expect("veth2", saw_packet_equaling(arp_response))
        ctrl.clear_arp_table()
        ctrl.add_arp_table_entry("192.168.1.1", "aa:bb:cc:dd:ee:ff")
        sendp(arp_querry, iface="veth2")
        result.assert_true()
        result_update = context.expect(
            "veth2", saw_packet_equaling(arp_response_update)
        )
        ctrl.update_arp_table_entry("192.168.1.1", "aa:bb:cc:dd:ee:11")
        sendp(arp_querry, iface="veth2")
        result_update.assert_true()
        ctrl.clear_arp_table()

    def test_arp_counters_table(self, context, ctrl):
        ctrl.clear_simple_table()
        ctrl.clear_arp_table()
        success, pre_counters = ctrl.get_arp_table_counters()
        assert len(pre_counters) == 0
        ctrl.add_arp_table_entry("192.168.1.1", "aa:bb:cc:dd:ee:ff")
        sendp(arp_querry, iface="veth2")
        post_result = []
        post_result.append(
            {
                "IP": "192.168.1.1",
                "Pkts": "1",
                "Bytes": "64",
            }
        )
        success, post_counters = ctrl.get_arp_table_counters()
        assert post_counters == post_result
        ctrl.reset_arp_counters()
        reset_result = []
        reset_result.append(
            {
                "IP": "192.168.1.1",
                "Pkts": "0",
                "Bytes": "0",
            }
        )
        success, reset_counters = ctrl.get_arp_table_counters()
        assert reset_counters == reset_result
        ctrl.reset_arp_counters()
        ctrl.clear_simple_table()
        ctrl.clear_arp_table()


@pytest.mark.bfrt
@pytest.mark.usefixtures("context", "ctrl")
class TestIngreEgressCounter:
    def test_ingress_egress_counter(self, context, ctrl):
        results = [context.expect("veth4", saw_packet_equaling(packet))]
        ctrl.clear_simple_table()
        ctrl.add_simple_table_entry(
            front_panel_regex.match("1/1"), front_panel_regex.match("1/2")
        )
        ctrl.reset_counter_egress()
        ctrl.reset_counter_ingress()
        res_i_pre, counter_ingress_pre = ctrl.get_counter_ingress("1/1")
        res_e_pre, counter_egress_pre = ctrl.get_counter_egress("1/2")
        sendp(packet, iface="veth2")

        protos = [
            "UNKNOWN",
            "ARP",
            "ICMP",
            "IP_OTHER",
            "UDP_OTHER",
            "PSR",
        ]

        expected_ingress_counter_pre = []
        expected_egress_counter_pre = []
        expected_ingress_counter_post = []
        expected_egress_counter_post = []
        for proto in protos:
            counter_egress_pre_entry = {
                "Port": "1/2",
                "Proto": "{}".format(proto),
                "Pkts": 0,
                "Bytes": 0,
            }
            counter_egress_post_entry = {
                "Port": "1/2",
                "Proto": "{}".format(proto),
                "Pkts": 0,
                "Bytes": 0,
            }
            counter_ingress_pre_entry = {
                "Port": "1/1",
                "Proto": "{}".format(proto),
                "Pkts": 0,
                "Bytes": 0,
            }
            counter_ingress_post_entry = {
                "Port": "1/1",
                "Proto": "{}".format(proto),
                "Pkts": 0,
                "Bytes": 0,
            }
            expected_ingress_counter_pre.append(counter_ingress_pre_entry)
            expected_egress_counter_pre.append(counter_egress_pre_entry)
            expected_ingress_counter_post.append(counter_ingress_post_entry)
            expected_egress_counter_post.append(counter_egress_post_entry)

        counter_egress_pre_entry = {
            "Port": "1/2",
            "Proto": "SPEAD",
            "Pkts": 0,
            "Bytes": 0,
        }
        counter_ingress_pre_entry = {
            "Port": "1/1",
            "Proto": "SPEAD",
            "Pkts": 0,
            "Bytes": 0,
        }
        counter_egress_post_entry = {
            "Port": "1/2",
            "Proto": "SPEAD",
            "Pkts": 1,
            "Bytes": 8114,
        }
        counter_ingress_post_entry = {
            "Port": "1/1",
            "Proto": "SPEAD",
            "Pkts": 1,
            "Bytes": 8114,
        }
        expected_ingress_counter_pre.append(counter_ingress_pre_entry)
        expected_egress_counter_pre.append(counter_egress_pre_entry)
        expected_ingress_counter_post.append(counter_ingress_post_entry)
        expected_egress_counter_post.append(counter_egress_post_entry)
        counter_ingress_pre.sort(key=lambda x: x["Proto"])
        counter_egress_pre.sort(key=lambda x: x["Proto"])
        expected_ingress_counter_pre.sort(key=lambda x: x["Proto"])
        expected_egress_counter_pre.sort(key=lambda x: x["Proto"])
        expected_ingress_counter_post.sort(key=lambda x: x["Proto"])
        expected_egress_counter_post.sort(key=lambda x: x["Proto"])
        res_i_post, counter_ingress_post = ctrl.get_counter_ingress("1/1")
        res_e_post, counter_egress_post = ctrl.get_counter_egress("1/2")
        counter_ingress_post.sort(key=lambda x: x["Proto"])
        counter_egress_post.sort(key=lambda x: x["Proto"])
        # results.append(expected_ingress_counter_pre is counter_ingress_pre)
        # results.append(expected_ingress_counter_post is counter_ingress_post)
        # results.append(expected_egress_counter_pre is counter_egress_pre)
        # results.append(expected_egress_counter_post is counter_egress_post)
        compare = [
            x for x in counter_ingress_pre if x in expected_ingress_counter_pre
        ]
        assert len(compare) == len(expected_ingress_counter_pre)
        compare = [
            x for x in counter_egress_pre if x in expected_egress_counter_pre
        ]
        assert len(compare) == len(expected_egress_counter_pre)
        compare = [
            x
            for x in counter_ingress_post
            if x in expected_ingress_counter_post
        ]
        assert len(compare) == len(expected_ingress_counter_post)
        compare = [
            x for x in counter_egress_post if x in expected_egress_counter_post
        ]
        assert len(compare) == len(expected_egress_counter_post)

        for result in results:
            assert result


@pytest.mark.bfrt
@pytest.mark.usefixtures("context", "ctrl")
class TestMultiplier:
    def test_saw_packet_multiplier_table(self, context, ctrl):

        result = context.expect("veth0", saw_packet_equaling(packet))
        result1 = context.expect("veth2", saw_packet_equaling(packet))
        ctrl.clear_multiplier()
        ctrl.clear_simple_table()
        ctrl.create_muticast_session(1)

        ctrl.add_ingress_to_multicast_session(
            front_panel_regex.match("1/2"), 1
        )
        ctrl.configure_multicast_session(
            1, 1, 1, 1, int("0x1FFF", 16), int("0x1EEE", 16)
        )
        ctrl.add_simple_table_entry(
            front_panel_regex.match("1/2"), front_panel_regex.match("1/1")
        )

        ctrl.add_port_to_multicast_session(
            1, 1, front_panel_regex.match("1/0")
        )
        ctrl.add_port_to_multicast_session(
            1, 1, front_panel_regex.match("1/1")
        )

        # success, table = ctrl.get_spead_multiplier_table_entries()
        # expected_table = []
        # expected_table.append(
        #    {"Frequency": "1", "Beam": "2", "Sub_array": "3", "session": "1"}
        # )
        # assert table == expected_table
        sendp(packet, iface="veth4")
        result.assert_true()
        result1.assert_true()
        ctrl.clear_simple_table()
