from scapy.all import *


class Spead(Packet):
    name = "SPEAD Packet"
    fields_desc = [
        LongField("spead_header", 5),
        IntField("high_heap_counter", 3),
        IntField("high_heap", 3),
        LongField("spead_payload_len", 8),
        LongField("ref_time", 5),
        LongField("frame_timestamp", 5),
        LongField("frew_channel", 5),
        IntField("pad", 3),
        ShortField("beam_no", 1),
        ShortField("frequency_no", 1),
        X3BytesField("pad2", 1),
        XByteField("sub_array", 1),
        IntField("station_no", 3),
        IntField("num_antennas", 3),
        StrLenField("data", "A" * 8000),
    ]
